#!/bin/bash

user=$syl
list=($(awk '{print}' <<< `ls -r -I 'metrics_agg_*' "/home/syl/log/" | head -n 60`))

# Inisialisasi variabel-variabel agregasi dengan nilai nol
mem_total_avg=0
mem_used_avg=0
mem_free_avg=0
mem_shared_avg=0
mem_buff_avg=0
mem_available_avg=0
swap_total_avg=0
swap_used_avg=0
swap_free_avg=0
path_size_avg=0

for i in "${list[@]}";
do
	path="/home/syl/log/$i";
        mem_total+=($(awk -F, 'NR==2 {print $1}' "$path"));
	mem_total_avg=$(("$mem_total_avg" + $(awk -F, 'NR==2 {print $1}' "$path")));
	mem_used+=($(awk -F, 'NR==2 {print $2}' "$path"));
	mem_used_avg=$(("$mem_used_avg" + $(awk -F, 'NR==2 {print $2}' "$path")));
	mem_free+=($(awk -F, 'NR==2 {print $3}' "$path"));
	mem_free_avg=$(("$mem_free_avg" + $(awk -F, 'NR==2 {print $3}' "$path")));
	mem_shared+=($(awk -F, 'NR==2 {print $4}' "$path"));
	mem_shared_avg=$(("$mem_shared_avg" + $(awk -F, 'NR==2 {print $4}' "$path")));
	mem_buff+=($(awk -F, 'NR==2 {print $5}' "$path"));
	mem_buff_avg=$(("$mem_buff_avg" + $(awk -F, 'NR==2 {print $5}' "$path")));
	mem_available+=($(awk -F, 'NR==2 {print $6}' "$path"));
	mem_available_avg=$(("$mem_available_avg" + $(awk -F, 'NR==2 {print $6}' "$path")));
    swap_total+=($(awk -F, 'NR==2 {print $7}' "$path"))
    swap_total_avg=$(echo "scale=2; $swap_total_avg + $(awk -F, 'NR==2 {print int($7)}' "$path")" | bc)
    swap_used+=($(awk -F, 'NR==2 {print $8}' "$path"))
    swap_used_avg=$(echo "scale=2; $swap_used_avg + $(awk -F, 'NR==2 {print int($8)}' "$path")" | bc)
    swap_free+=($(awk -F, 'NR==2 {print $9}' "$path"))
    swap_free_avg=$(echo "scale=2; $swap_free_avg + $(awk -F, 'NR==2 {print int($9)}' "$path")" | bc)
    path_dir=$(awk -F, 'NR==2 {print $10}' "$path")
    path_size+=($(awk -F'[M,]' 'NR==2 {print $11}' "$path"))
    path_size_avg=$(("$path_size_avg" + $(awk -F'[M,]' 'NR==2 {print $11}' "$path")))
done

mem_total_avg=$(echo "scale=2; $mem_total_avg / 60" | bc)
mem_used_avg=$(echo "scale=2; $mem_used_avg / 60" | bc)
mem_free_avg=$(echo "scale=2; $mem_free_avg / 60" | bc)
mem_shared_avg=$(echo "scale=2; $mem_shared_avg / 60" | bc)
mem_buff_avg=$(echo "scale=2; $mem_buff_avg / 60" | bc)
mem_available_avg=$(echo "scale=2; $mem_available_avg / 60" | bc)
swap_total_avg=$(echo "scale=2; $swap_total_avg / 60" | bc)
swap_used_avg=$(echo "scale=2; $swap_used_avg / 60" | bc)
swap_free_avg=$(echo "scale=2; $swap_free_avg / 60" | bc)
path_size_avg=$(echo "scale=2; $path_size_avg / 60" | bc)

mem_total_sorted=($(for i in "${mem_total[@]}"; do echo "$i"; done | sort -n))
mem_used_sorted=($(for i in "${mem_used[@]}"; do echo "$i"; done | sort -n))
mem_free_sorted=($(for i in "${mem_free[@]}"; do echo "$i"; done | sort -n))
mem_shared_sorted=($(for i in "${mem_shared[@]}"; do echo "$i"; done | sort -n))
mem_buff_sorted=($(for i in "${mem_buff[@]}"; do echo "$i"; done | sort -n))
mem_available_sorted=($(for i in "${mem_available[@]}"; do echo "$i"; done | sort -n))
swap_total_sorted=($(for i in "${swap_total[@]}"; do echo "$i"; done | sort -n))
swap_used_sorted=($(for i in "${swap_used[@]}"; do echo "$i"; done | sort -n))
swap_free_sorted=($(for i in "${swap_free[@]}"; do echo "$i"; done | sort -n))
path_size_sorted=($(for i in "${path_size[@]}"; do echo "$i"; done | sort -n | awk '{ printf "%.2f\n", $1 }'))

file=$(printf "metrics_agg_%(%Y%m%d%H)T\n" $(( $(printf "%(%s)T") - 60 * 60 )))
file_dir="/home/syl/log/$file"".log"

echo "type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" >> "$file_dir"
echo "minimum,${mem_total_sorted[0]},${mem_used_sorted[0]},${mem_free_sorted[0]},${mem_shared_sorted[0]},${mem_buff_sorted[0]},${mem_available_sorted[0]},${swap_total_sorted[0]},${swap_used_sorted[0]},${swap_free_sorted[9]},$path_dir,${path_size_sorted[0]}M" >> "$file_dir"
echo "maximum,${mem_total_sorted[59]},${mem_used_sorted[59]},${mem_free_sorted[59]},${mem_shared_sorted[59]},${mem_buff_sorted[59]},${mem_available_sorted[59]},${swap_total_sorted[59]},${swap_used_sorted[59]},${swap_free_sorted[59]},$path_dir,${path_size_sorted[59]}M" >> "$file_dir"
echo "average,$mem_total_avg,$mem_used_avg,$mem_free_avg,$mem_shared_avg,$mem_buff_avg,$mem_available_avg,$swap_total_avg,$swap_used_avg,$swap_free_avg,$path_dir,$path_size_avg""M" >> "$file_dir"
