#!/bin/bash

# Mendapatkan waktu saat ini dan memformatnya sebagai {YmdHms}
timestamp=$(date +'%Y%m%d%H%M%S')

# Membuat nama file log sesuai format
log_file="/home/syl/Sisop/modul1/soal4/metrics_${timestamp}.log"

# Mendapatkan informasi RAM dengan perintah 'free -m'
ram_info=$(free -m)

# Mendapatkan size dari directory target dengan perintah 'du -sh'
target_path="/home/syl/"
directory_size=$(du -sh "$target_path")

# Menyimpan metrics ke dalam file log
echo "$ram_info" >> "$log_file"
echo "$directory_size" >> "$log_file"
