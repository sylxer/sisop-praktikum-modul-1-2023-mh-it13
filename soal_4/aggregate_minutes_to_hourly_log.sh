#!/bin/bash

# Mendapatkan waktu saat ini dan memformatnya sebagai {YmdH}
timestamp=$(date +'%Y%m%d%H')

# Membuat nama file log aggregasi sesuai format
aggregate_log_file="/home/syl/Sisop/modul1/soal4/metrics_agg_${timestamp}.log"

# Mendapatkan daftar file log metrics dari jam-jam sebelumnya
logs_to_aggregate=$(find /home/syl/Sisop/modul1/soal4/ -name "metrics_*" -type f)

# Inisialisasi variabel untuk agregasi
mem_total_min=99999999
mem_total_max=0
mem_total_sum=0
mem_used_min=99999999
mem_used_max=0
mem_used_sum=0

# Loop untuk mengumpulkan data dari file log
for log in $logs_to_aggregate
do
    # Mendapatkan data dari file log
    ram_info=$(sed -n '2p' "$log")
    directory_size=$(sed -n '3p' "$log")

    # Memisahkan data menjadi kolom-kolom
    IFS=',' read -ra ram_metrics <<< "$ram_info"
    IFS=',' read -ra size_metrics <<< "$directory_size"

    # Memperbarui variabel agregasi
    mem_total=${ram_metrics[1]}
    mem_used=${ram_metrics[2]}
    mem_total_sum=$((mem_total_sum + mem_total))
    mem_used_sum=$((mem_used_sum + mem_used))

    # Membandingkan dan memperbarui nilai minimum dan maksimum
    if (( mem_total < mem_total_min )); then
        mem_total_min=$mem_total
    fi

    if (( mem_total > mem_total_max )); then
        mem_total_max=$mem_total
    fi

    if (( mem_used < mem_used_min )); then
        mem_used_min=$mem_used
    fi

    if (( mem_used > mem_used_max )); then
        mem_used_max=$mem_used
    fi
done

# Menghitung nilai rata-rata
mem_total_avg=$((mem_total_sum / $(wc -l <<< "$logs_to_aggregate")))
mem_used_avg=$((mem_used_sum / $(wc -l <<< "$logs_to_aggregate")))

# Menyimpan hasil agregasi ke dalam file log aggregasi
echo "type,mem_total_min,mem_total_max,mem_total_avg,mem_used_min,mem_used_max,mem_used_avg" > "$aggregate_log_file"
echo "hourly,$mem_total_min,$mem_total_max,$mem_total_avg,$mem_used_min,$mem_used_max,$mem_used_avg" >> "$aggregate_log_file"
